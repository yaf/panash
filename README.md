# Panash

_Merci Florent Biville ([@fbiville](https://twitter.com/fbiville)) pour le nom ;-)_
Ce projet à pour objet de construire un outil (ou une série d'outils ?) pour publier.

- À partir de markdown, générer du HTML
- Les documents d'un répertoire
- Uniquement les documents modifié depuis la dernière fois

## Questions

- Faut-il avoir une mécanique de mis à jour d'index-liste, et les flux (rss/atom) ?
- Publier quelque part ? Non lié à git je pense ici (outil externe)


## Pense bête

`pandoc --template=template.html -c pandoc.css -H head.html -B header.html -A footer.html src/index.md -o example3.html`


## Développement

Installer https://github.com/koalaman/shellcheck

via Cabal:

```sh
cabal install shellchek
~/.cabal/bin/shellcheck pana.sh
```


