# TODO

- construire un executable, un truc facile à installer et utiliser (?)
- faciliter la gestion des templates
- faire des tests unitaires (https://github.com/pgrange/bash_unit)
- construire un flux RSS automatiquement
- construire un sitemap.xml automatiquement
- avoir un setup permettant d'installer et configurer panash
