#!/bin/sh
#

# Ligne complète pour pandoc:
# pandoc --template=lib/template.html -c lib/pandoc.css -H lib/head.html -B lib/header.html -A lib/footer.html "$mdfile" -o "$htmlfile"

MARKDOWN_FILES=$(find content -name \*.md)
ASCIIDOC_FILES=$(find content -name \*.txt)
FEED_FILE=content/feed.xml
KEYWORDS=keywords.md

cat "" > $KEYWORDS

for mdfile in $MARKDOWN_FILES; do
  htmlfile="${mdfile%%.*}.html"
  if [ ! -f "$htmlfile" ] || [ "$mdfile" != "$htmlfile" ]; then
    echo "--> generate with pandoc '$htmlfile' from '$mdfile'"
    pandoc --template=lib/template.html -c lib/pandoc.css "$mdfile" -o "$htmlfile"
  fi
  pandoc --template=lib/extract-keywords.md "$mdfile" >> $KEYWORDS
done

for asciidocfile in $ASCIIDOC_FILES; do
  htmlfile="${asciidocfile%%.*}.html"
  if [ ! -f "$htmlfile" ] || [ "$asciidocfile" != "$htmlfile" ]; then
    echo "--> generate with asciidoc'$htmlfile' from '$asciidocfile'"
    asciidoc -o "$htmlfile" -b html5 "$mdfile" 
  fi
done

for line in $(cat $KEYWORDS); do
  directory=content/$(echo $line | awk -F":" '{print $1}')
  echo $directory
  fichier=/$(echo $line | awk -F":" '{print $2}' | awk -F"content|index.md" '{print $1}')
  echo $fichier
  titre=$(echo $line | awk -F":" '{print $3}')
  echo $titre
  if [ ! -d $directory ]; then
    mkdir -p $directory
  fi
  if [ ! -f $directory/index.md ]; then
    touch $directory/index.md
  else
    echo "" > $directory/index.md
  fi

  echo "- [$titre]($fichier)" >> $directory/index.md

  pandoc --template=lib/template.html -c lib/pandoc.css "$directory/index.md" -o "$directory/index.html"
done

echo "--> copy html files to public dir"
# HTML_FILES=$(find content -name \*.html)
# cp --parents content/*.html public
# (cd content && cp -r --parent . ../public)
# (cd content && find . -name \*.html -exec cp --parents {} ../public \;)

echo "Build atom"

LAST_CHANGES=$(git log --pretty="format:" --name-only |  grep  "content/posts/20*" | grep ".html" | uniq | tac | head -10)
echo "liste des 10 derniers fichiers qui ont changés : $LAST_CHANGES"


ITEMS=""
for change in $LAST_CHANGES
do
  titre=$(awk -F"<h1>|</h1>" '{for(i=2;i<=NF;i+=2){print $i}}' RS="" $change)
  lien=https://elsif.fr/$(echo $change | awk  '{gsub(/content\//, ""); print $1}')
  updated=$(date -Ins -r $change)
  #summary=$(xmllint --html --noent --xpath "//article" $change)
  summary=""

  ITEMS="$ITEMS
<entry xml:lang='fr'><title>$titre</title><link href='$lien' rel='alternate' type='text/html'/><updated>$updated</updated><id>tag:$(shasum $change | awk -F ' ' '{print $1}')</id><summary type='HTML'>$summary</summary><author><name>Yannick François</name><email>yaf@elsif.fr</email></author></entry>

"
done


cat << EOF > $FEED_FILE
<?xml version="1.0" encoding="utf-8"?>

<feed xmlns="http://www.w3.org/2005/Atom">
  <title>Elsif.fr - Yannick François</title>
  <subtitle>les dernières mise à jour du site</subtitle>
  <link href="http://elsif.fr/" />
  <id>tag:elsif.fr</id>
  <updated>$(date -R)</updated>

  $ITEMS
</feed>
EOF


exit 0
